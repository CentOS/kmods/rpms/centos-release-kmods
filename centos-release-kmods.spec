Name:             centos-release-kmods
Version:          9
Release:          4%{?dist}
Summary:          CentOS Kmods SIG package repositories

License:          GPLv2
URL:              https://sigs.centos.org/kmods

Source0:          RPM-GPG-KEY-CentOS-SIG-Kmods
Source1:          centos-kmods.repo

BuildArch:        noarch

Recommends:       epel-release


%description
This package provides the package repository files for CentOS Kmods SIG.


%prep


%build


%install
%{__install} -m 644 -D -t %{buildroot}%{_sysconfdir}/pki/rpm-gpg %{SOURCE0}
%{__install} -m 644 -D -t %{buildroot}%{_sysconfdir}/yum.repos.d %{SOURCE1}


%clean
%{__rm} -rf %{buildroot}


%files
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Kmods
%config(noreplace) %{_sysconfdir}/yum.repos.d/centos-kmods.repo
%ghost %{_sysconfdir}/yum.repos.d/centos-kmods-userspace.repo


%changelog
* Tue Dec 05 2023 Peter Georg <peter.georg@physik.uni-regensburg.de> - 9-4
- Remove unused testing repos
- Remove reference to Stream in repo name
- Remove Requires: centos-stream-release
- Use a single config for all repos
- Use packages-userspace from el9

* Mon May 15 2023 Peter Georg <peter.georg@physik.uni-regensburg.de> - 9-3
- Enable gpgcheck for testing repositories

* Tue Feb 14 2023 Peter Georg <peter.georg@physik.uni-regensburg.de> - 9-2
- Add new packages-userspace repos

* Sat Mar 12 2022 Peter Georg <peter.georg@physik.uni-regensburg.de> - 9-1
- Refresh GPG public key
- Change versioning to match major release

* Mon Feb 07 2022 Peter Georg <peter.georg@physik.uni-regensburg.de> - 2-5
- Add metadata_expire=6h option to repo configs
- Add repo_gpgcheck=0 to repo configs
- Add countme=1 to selected repo configs
- Sync format with centos-release repo configs

* Thu Jan 27 2022 Peter Georg <peter.georg@physik.uni-regensburg.de> - 2-4
- Add Recommends epel-release

* Mon Jan 24 2022 Peter Georg <peter.georg@physik.uni-regensburg.de> - 2-3
- Add centos-kmods-debug repository
- $contentdir not defined in CentOS Stream 9

* Thu Jan 20 2022 Peter Georg <peter.georg@physik.uni-regensburg.de> - 2-2
- Use a single config file for all repos
- Disable centos-kmods-rebuild by default

* Fri Dec 24 2021 Peter Georg <peter.georg@physik.uni-regensburg.de> - 2-1
- Initial release
